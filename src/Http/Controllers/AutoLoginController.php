<?php

namespace Helium\AutoLogin\Http\Controllers;

use Helium\AutoLogin\AutoLogin;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Illuminate\Support\Facades\Auth;

class AutoLoginController extends Controller
{
    public function generate(Request $request)
    {
        return AutoLogin::getUrl($request->user(), $request->expiration, $request->guard);
    }

    public function autologin(Request $request, string $id)
    {
        if (!$request->hasValidSignature()) {
            throw new InvalidSignatureException();
        }

        if (isset($request->guard)) {
            Auth::shouldUse($request->guard);
        }

        if (Auth::loginUsingId($id)) {
            return redirect()->route('home');
        } else {
            throw new AuthenticationException();
        }
    }
}