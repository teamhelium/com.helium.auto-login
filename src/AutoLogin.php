<?php

namespace Helium\AutoLogin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class AutoLogin
{
    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable|string $authenticatable
     * @param \DateTimeInterface|\DateInterval|int|null $expiration
     * @param string|null $guard
     * @return string
     */
    public static function getUrl($authenticatable, $expiration = null, $guard = null)
    {
        if (!$expiration) {
            $expiration = Carbon::now()->addSeconds(30);
        } elseif (intval($expiration)) {
            $expiration = Carbon::createFromTimestamp(intval($expiration));
        }

        if ($authenticatable instanceof Model) {
            $id = $authenticatable->getKey();
        } else {
            $id = $authenticatable;
        }

        $params = [
            'id' => $id
        ];

        if (isset($guard)) {
            $params['guard'] = $guard;
        }

        return URL::temporarySignedRoute('autologin', $expiration, $params);
    }
}