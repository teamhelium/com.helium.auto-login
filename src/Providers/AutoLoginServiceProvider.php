<?php

namespace Helium\AutoLogin\Providers;

use Helium\AutoLogin\Http\Controllers\AutoLoginController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AutoLoginServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (!$this->app->routesAreCached())
        {
            Route::get('autologin/generate', [AutoLoginController::class, 'generate'])
                ->name('autologin.generate')
                ->middleware(['web', 'auth']);

            Route::get('api/autologin/generate', [AutoLoginController::class, 'generate'])
                ->name('autologin.generate.api')
                ->middleware(['api', 'auth']);

            Route::get('autologin/{id}', [AutoLoginController::class, 'autologin'])
                ->name('autologin')
                ->middleware(['web', 'guest']);
        }
    }
}