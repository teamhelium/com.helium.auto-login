<?php

namespace Tests\TestCases;

use Helium\AutoLogin\Providers\AutoLoginServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    //TODO: Setup auth for app

    protected function getPackageProviders($app)
    {
        return [
            AutoLoginServiceProvider::class
        ];
    }
}