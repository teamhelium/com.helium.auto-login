<?php

namespace Tests\TestCases;

use Helium\AutoLogin\AutoLogin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Tests\Assets\Auth\NoUserGuard;
use Tests\Assets\Models\TestUser;

class AutoLoginRoutesTest extends TestCase
{
    protected function testGenerateEndpoint(string $route)
    {
        /**
         * Setup
         */
        Route::get('login', function(){})->name('login');

        /** @var TestUser $user */
        $user = new TestUser(['id' => 1]);

        /**
         * Test Unauthenticated
         */
        if ($route == 'autologin.generate.api') {
            $response = $this->getJson(route($route));
            $response->assertUnauthorized();
        } else {
            $response = $this->get(route($route));
            $response->assertRedirect(route('login'));
        }

        /**
         * Test default
         */
        $this->actingAs($user);

        if ($route == 'autologin.generate.api') {
            $response = $this->getJson(route($route));
        } else {
            $response = $this->get(route($route));
        }
        $response->assertSuccessful();

        $url = $response->getContent();
        $this->assertTrue(Str::contains($url, '/autologin/1'));
        $this->assertTrue(Str::contains($url, 'expires='));
        $this->assertTrue(Str::contains($url, 'signature='));
        $this->assertFalse(Str::contains($url, 'guard='));

        /**
         * Test arguments
         */
        $expires = now()->addMinute()->getTimestamp();

        if ($route == 'autologin.generate.api') {
            $response = $this->getJson(route($route, [
                'expiration' => $expires,
                'guard' => 'customGuard'
            ]));
        } else {
            $response = $this->get(route($route, [
                'expiration' => $expires,
                'guard' => 'customGuard'
            ]));
        }
        $response->assertSuccessful();

        $url = $response->getContent();
        $this->assertTrue(Str::contains($url, '/autologin/1'));
        $this->assertTrue(Str::contains($url, 'expires=' . $expires));
        $this->assertTrue(Str::contains($url, 'signature='));
        $this->assertTrue(Str::contains($url, 'guard=customGuard'));
    }

    public function testGenerate()
    {
        $this->testGenerateEndpoint('autologin.generate');
    }

    public function testGenerateApi()
    {
        $this->testGenerateEndpoint('autologin.generate.api');
    }

    public function testAutologin()
    {
        /**
         * Setup
         */
        Route::get('login', function(){})->name('login');
        Route::get('home', function(){})->name('home');

        Auth::partialMock();
        Auth::shouldReceive('guard')
            ->andReturn(new NoUserGuard());

        /** @var TestUser $user */
        $user = new TestUser(['id' => 1]);

        /**
         * Test Invalid Signature
         */
        $response = $this->get(route('autologin', 1));
        $response->assertForbidden();

        /**
         * Test Tampered URL
         */
        $url = Str::of(AutoLogin::getUrl($user))
            ->replace('autologin/1', 'autologin/2')
            ->__toString();
        $response = $this->get($url);
        $response->assertForbidden();

        /**
         * Test Invalid User
         */
        Auth::shouldReceive('loginUsingId')
            ->once()
            ->andReturn(false);

        $response = $this->get(AutoLogin::getUrl($user));
        $response->assertRedirect(route('login'));

        /**
         * Test Success
         */
        Auth::shouldReceive('loginUsingId')
            ->once()
            ->andReturn($user);

        $response = $this->get(AutoLogin::getUrl($user, now()->addMinute()->getTimestamp(), 'customGuard'));
        $response->assertRedirect(route('home'));
        $response->assertCookie('laravel_session');
    }
}