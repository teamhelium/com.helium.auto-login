<?php

namespace Tests\TestCases;

use Helium\AutoLogin\AutoLogin;
use Illuminate\Support\Str;
use Tests\Assets\Models\TestUser;

class AutoLoginTest extends TestCase
{
    public function testGetUrl()
    {
        //Test with default values
        $url = AutoLogin::getUrl(1);
        $this->assertTrue(Str::contains($url, '/autologin/1'));
        $this->assertTrue(Str::contains($url, 'expires='));
        $this->assertTrue(Str::contains($url, 'signature='));
        $this->assertFalse(Str::contains($url, 'guard='));

        //Test with custom expiration and guard
        $expires = now()->addMinute();
        $url = AutoLogin::getUrl(1, $expires, 'customGuard');
        $this->assertTrue(Str::contains($url, '/autologin/1'));
        $this->assertTrue(Str::contains($url, 'expires=' . $expires->timestamp));
        $this->assertTrue(Str::contains($url, 'signature='));
        $this->assertTrue(Str::contains($url, 'guard=customGuard'));

        //Test with model
        $user = new TestUser(['id' => 1]);
        $url = AutoLogin::getUrl($user);
        $this->assertTrue(Str::contains($url, '/autologin/1'));
        $this->assertTrue(Str::contains($url, 'expires='));
        $this->assertTrue(Str::contains($url, 'signature='));
        $this->assertFalse(Str::contains($url, 'guard='));
    }
}