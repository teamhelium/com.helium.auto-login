<?php

namespace Tests\Assets\Auth;

class NoUserGuard
{
    public function check() {
        return false;
    }
}