<?php

namespace Tests\Assets\Models;

use Illuminate\Foundation\Auth\User;

class TestUser extends User
{
    protected $guarded = [];
}